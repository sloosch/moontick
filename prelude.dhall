let AlertValueGen
    : Type
    = < Points :
          List Double
      | LinearSteps :
          { from : Double, to : Double, step : Double }
      | Fibonacci :
          { from : Double, to : Double, ratios : Natural }
      >

let AlertRule
    : Type
    = < GreaterThan : AlertValueGen | LowerThan : AlertValueGen >

let Coin : Type = { id : Natural, name : Text, alerts : List AlertRule }

let Config : Type = { coins : List Coin }

let noAlerts = [] : List AlertRule

in  { Coin =
        Coin
    , AlertRule =
        AlertRule
    , AlertValueGen =
        AlertValueGen
    , Config =
        Config
    , noAlerts =
        noAlerts
    }
