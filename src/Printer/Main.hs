module Main where

import Control.Applicative ((<|>))
import qualified Control.Concurrent as C
import qualified Control.Concurrent.Async as C
import qualified Control.Concurrent.Thread.Delay as CT
import Control.Monad (void, when)
import Data.Foldable (Foldable (foldl'))
import Data.List ((\\))
import qualified Data.Map.Strict as M
import Formatting (fixed, fprintLn, stext, (%))
import Moontick.Internal.Coin (Coin (..))
import qualified Moontick.Internal.MessageChannel as MC
import qualified Moontick.Internal.TickerMessage as TM
import System.Clock (Clock (Monotonic), diffTimeSpec, getTime, toNanoSecs)
import System.IO (BufferMode (NoBuffering), hSetBuffering, stdout)
import qualified System.Posix.Signals as S

tickerPrinterSystem :: [Coin] -> MC.MessageQueue -> IO ()
tickerPrinterSystem initialCoins messages = do
  signal <- C.newEmptyMVar
  void $ S.installHandler S.sigUSR1 (S.Catch $ void $ C.tryPutMVar signal ()) Nothing
  cyclePrinter (C.takeMVar signal) M.empty (cycle initialCoins)
  where
    printer coin = \case
      Nothing -> fprintLn (stext % " NA") (_coinName coin)
      Just (curr, prev) ->
        let coinName = _coinName coin
            currPrice = TM._tickerPrice curr
            prevPrice = TM._tickerPrice prev
            currPercent = TM._tickerPercent curr
            updown = case compare currPrice prevPrice of
              GT -> "▲"
              LT -> "▼"
              EQ -> "•"
         in do
              fprintLn
                (stext % " " % fixed 4 % stext % " (" % fixed 2 % "%)")
                coinName
                currPrice
                updown
                currPercent

    handleCoinMessages signal timeout initial f
      | timeout <= 0 = pure initial
      | otherwise = do
        enterTime <- getTime Monotonic
        let work =
              C.Concurrently (Just <$> MC.awaitMessage messages)
                <|> C.Concurrently (Nothing <$ CT.delay timeout)
                <|> C.Concurrently (Nothing <$ signal)
        C.runConcurrently work >>= \case
          Nothing -> pure initial
          Just message -> do
            exitTime <- getTime Monotonic
            let diffTime = toNanoSecs (diffTimeSpec exitTime enterTime) `div` 1000
            a <- f initial message
            handleCoinMessages signal (timeout - diffTime) a f

    cyclePrinter :: IO () -> M.Map Coin (TM.TickerMessage, TM.TickerMessage) -> [Coin] -> IO ()
    cyclePrinter signal prevMessages = \case
      (activeCoin : cs) -> do
        let prevCoinMessage = M.lookup activeCoin prevMessages
        printer activeCoin prevCoinMessage
        (newMessages, newCoins) <- handleCoinMessages signal 5000000 (prevMessages, cs) \(newMessages', newCoins') -> \case
          MC.MessageTicker coin tickerMessages -> do
            when (coin == activeCoin) $ printer activeCoin $ Just tickerMessages
            pure (M.insert coin tickerMessages newMessages', newCoins')
          MC.MessageCoins newCoins ->
            let currentCoins = M.keys newMessages'
                removedCoins = currentCoins \\ newCoins
                cleanedUpMessages = foldl' (flip M.delete) newMessages' removedCoins
             in pure (cleanedUpMessages, cycle newCoins)
          _ -> pure (newMessages', newCoins')
        cyclePrinter signal newMessages newCoins
      _ -> pure ()

main :: IO ()
main = do
  hSetBuffering stdout NoBuffering
  MC.runCLIApp $
    MC.withMessages
      [ tickerPrinterSystem
      ]
