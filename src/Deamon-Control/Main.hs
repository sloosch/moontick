{-# LANGUAGE ApplicativeDo #-}

module Main where

import Control.Applicative ((<**>))
import Formatting (fprintLn, string, (%))
import qualified Moontick.Deamon.Control as Ctrl
import Moontick.Internal.Coin (Coin (..))
import qualified Moontick.Internal.Notification as N
import qualified Options.Applicative as O

alertParser :: O.Parser N.Alert
alertParser = do
  value <- O.option O.auto (O.long "value" <> O.short 'v')
  coinName <- O.strOption (O.long "name" <> O.short 'n')
  rule <- O.flag N.GreaterThan N.LowerThan (O.long "lowerThan")
  pure $
    N.Alert
      { _alertCoin = Coin {_coinId = 666, _coinName = coinName},
        _alertRule = rule value,
        _alertSilent = False
      }

actionParser :: O.Parser Ctrl.Action
actionParser =
  O.subparser $
    O.command "send-alert" (O.info (Ctrl.SendAlert <$> alertParser) (O.progDesc "Send an alert"))
      <> O.command "mute" (O.info (pure Ctrl.Mute) (O.progDesc "Mute all alerts and do not receive any alerts from the deamon"))
      <> O.command "unmute" (O.info (pure Ctrl.Unmute) (O.progDesc "Unmute all alerts and receive alerts again"))

main :: IO ()
main = do
  action <- O.execParser (O.info (actionParser <**> O.helper) O.fullDesc)
  Ctrl.withControl \control ->
    Ctrl.sendAction control action >>= \case
      Nothing -> fprintLn "Could not send Action"
      Just resp -> fprintLn ("Done (" % string % ")") (show resp)