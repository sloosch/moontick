{-# LANGUAGE ApplicativeDo #-}

module Main where

import Control.Monad (forever)
import Data.List.NonEmpty (NonEmpty)
import qualified Data.List.NonEmpty as NEL
import Data.Text (Text, intercalate)
import Formatting (sformat, stext, fixed, (%))
import Moontick.Internal.Coin (Coin (..))
import qualified Moontick.Internal.MessageChannel as MC
import qualified Moontick.Internal.Notification as N
import qualified Moontick.Pushover as Pushover
import qualified Options.Applicative as O
import Control.Applicative ((<**>))

waitForAlerts :: MC.MessageQueue -> IO (NonEmpty N.Alert)
waitForAlerts messages = go
  where
    go =
      MC.awaitMessage messages >>= \case
        MC.MessageAlerts alerts -> pure alerts
        _ -> go

pushoverSystem :: Pushover.Config -> [Coin] -> MC.MessageQueue -> IO ()
pushoverSystem config _ messages = forever go
  where
    go :: IO ()
    go = waitForAlerts messages >>= Pushover.send config . alertsToPushoverMessage

    alertsToPushoverMessage :: NonEmpty N.Alert -> Pushover.Message
    alertsToPushoverMessage alerts =
      let title = intercalate "," $ NEL.toList $ formatAlertTitle <$> alerts
          body = intercalate "\n" $ NEL.toList $ formatAlertMessage <$> alerts
       in Pushover.Message
            { _pushoverTitle = title,
              _pushoverBody = body
            }

    formatAlertTitle :: N.Alert -> Text
    formatAlertTitle N.Alert {..} = sformat (stext % " Price") (_coinName _alertCoin)

    formatAlertMessage :: N.Alert -> Text
    formatAlertMessage N.Alert {..} = case _alertRule of
      N.GreaterThan v ->
        sformat
          (stext % " has raised above " % fixed 4)
          (_coinName _alertCoin)
          v
      N.LowerThan v ->
        sformat
          (stext % " has dropped below " % fixed 4)
          (_coinName _alertCoin)
        v

configParser :: O.Parser (Maybe MC.ChannelAddress, Pushover.Config)
configParser = do
  address <- O.optional $ O.argument O.str (O.metavar "ADDRESS")
  token <- O.strOption (O.long "token" <> O.short 't' <> O.metavar "TOKEN")
  user <- O.strOption (O.long "user" <> O.short 'u' <> O.metavar "USER")
  pure
    ( address,
      Pushover.Config
        { _pushoverToken = token,
          _pushoverUser = user
        }
    )

main :: IO ()
main = do
  (address, config) <- O.execParser (O.info (configParser <**> O.helper) O.fullDesc)
  MC.runApp address $
    MC.withMessages
      [pushoverSystem config]
