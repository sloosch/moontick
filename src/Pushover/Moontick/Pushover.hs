{-# LANGUAGE TypeApplications #-}

module Moontick.Pushover
  ( send,
    Config (..),
    Message (..),
  )
where

import Control.Exception (SomeException, try)
import Data.Text (Text)
import Formatting (fprintLn, stext, string, (%))
import Network.HTTP.Req
  ( POST (POST),
    ReqBodyUrlEnc (..),
    defaultHttpConfig,
    https,
    ignoreResponse,
    req,
    runReq,
    (/:),
    (=:),
  )

data Config = Config
  { _pushoverToken :: Text,
    _pushoverUser :: Text
  }

data Message = Message
  { _pushoverTitle :: Text,
    _pushoverBody :: Text
  }

send :: Config -> Message -> IO ()
send Config {..} Message {..} =
  let params =
        "token" =: _pushoverToken
          <> "user" =: _pushoverUser
          <> "message" =: _pushoverBody
          <> "title" =: _pushoverTitle
   in do
        res <-
          try @SomeException $
            runReq defaultHttpConfig $
              req
                POST
                (https "api.pushover.net" /: "1" /: "messages.json")
                (ReqBodyUrlEnc params)
                ignoreResponse
                mempty
        case res of
          Left e -> fprintLn ("Could not send pushover message: " % string) (show e)
          Right _ -> fprintLn ("Sent pushover message: " % stext) _pushoverTitle