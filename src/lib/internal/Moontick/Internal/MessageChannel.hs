module Moontick.Internal.MessageChannel
  ( Message (..),
    sendTicker,
    sendCoins,
    sendAlerts,
    withServer,
    Server,
    withMessages,
    awaitMessage,
    MessageQueue,
    Transport (..),
    ChannelAddress,
    App,
    runApp,
    runCLIApp,
  )
where

import qualified Control.Concurrent.Async as C
import qualified Control.Concurrent.STM as C
import Control.Monad (forever, replicateM)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.Cont (ContT (..), evalContT)
import Data.Function ((&))
import qualified Data.IORef as Ref
import Data.List (uncons)
import Data.List.NonEmpty (NonEmpty (..))
import qualified Data.List.NonEmpty as NEL
import Data.Text (Text, pack)
import Data.Text.Lazy (unpack)
import Formatting (format, fprintLn, int, stext, string, (%))
import Moontick.Internal.Coin (Coin)
import Moontick.Internal.Message (IsMessage (..))
import qualified Moontick.Internal.Message as M
import qualified Moontick.Internal.Notification as N
import qualified Moontick.Internal.TickerMessage as TM
import System.Environment (getArgs)
import qualified System.ZMQ4 as Z

data Message
  = MessageTicker Coin (TM.TickerMessage, TM.TickerMessage)
  | MessageAlerts (NonEmpty N.Alert)
  | MessageCoins [Coin]
  deriving (Show)

newtype InitMessage = InitMessage [Coin] deriving (Show)

instance IsMessage Message where
  cborMessageEncoding m = case m of
    MessageTicker coin (curr, prev) ->
      M.encodeWord 0
        <> cborMessageEncoding coin
        <> cborMessageEncoding curr
        <> cborMessageEncoding prev
    MessageAlerts alerts ->
      M.encodeWord 1 <> M.cborMessageEncodingList alerts
    MessageCoins coins ->
      M.encodeWord 2 <> M.cborMessageEncodingList coins
  cborMessageDecoding =
    M.decodeWord >>= \case
      0 -> do
        coin <- cborMessageDecoding
        curr <- cborMessageDecoding
        prev <- cborMessageDecoding
        pure $ MessageTicker coin (curr, prev)
      1 ->
        M.cborMessageDecodingList
          >>= ( \case
                  Nothing -> fail "List of alerts was empty"
                  Just alerts -> pure $ MessageAlerts alerts
              )
            . NEL.nonEmpty
      2 -> MessageCoins <$> M.cborMessageDecodingList
      badTag -> fail $ "Got a bad tag while decoding message: " <> show badTag

instance IsMessage InitMessage where
  cborMessageEncoding (InitMessage coins) = M.cborMessageEncodingList coins
  cborMessageDecoding = InitMessage <$> M.cborMessageDecodingList

type ChannelAddress = Text

newtype Server = Server (Message -> IO ())

newtype MessageQueue = MessageQueue (C.TQueue Message)

data Transport
  = TCP ChannelAddress Int
  | IPC
  deriving (Show)

data TransportDestination
  = DestinationInit
  | DestinationMessage
  deriving (Show)

type App = Transport -> IO ()

runApp :: Maybe ChannelAddress -> App -> IO ()
runApp maddress act =
  let transport = case maddress of
        Just address -> TCP address 5555
        Nothing -> IPC
   in do
        fprintLn ("Using " % string % " as transport") (show transport)
        act transport

runCLIApp :: App -> IO ()
runCLIApp act = do
  args <- getArgs
  runApp (pack . fst <$> uncons args) act

transportAddress :: Transport -> TransportDestination -> String
transportAddress transport destination = unpack case transport of
  TCP address port -> format ("tcp://" % stext % ":" % int) address case destination of
    DestinationMessage -> port
    DestinationInit -> port + 1
  IPC -> format ("ipc:///tmp/moontick_" % stext) case destination of
    DestinationMessage -> "message"
    DestinationInit -> "init"

printWithContext :: forall a. Show a => String -> a -> IO ()
printWithContext context a = putStrLn $ context <> ":" <> show a

withMessages :: [[Coin] -> MessageQueue -> IO ()] -> App
withMessages acts transport = evalContT do
  context <- ContT Z.withContext
  subSock <- ContT $ Z.withSocket context Z.Sub
  dealerSock <- ContT $ Z.withSocket context Z.Dealer
  liftIO do
    Z.connect subSock $ transportAddress transport DestinationMessage
    Z.subscribe subSock ""
    Z.connect dealerSock $ transportAddress transport DestinationInit
    Z.sendMulti dealerSock ("" :| ["init"])
    Z.receiveMulti dealerSock >>= \case
      ["", msg] ->
        M.decodeMessage msg & either (printWithContext "Init failed") \(InitMessage initCoins) -> do
          queues <- replicateM (length acts) C.newTQueueIO
          let loop =
                forever do
                  r <- Z.receive subSock
                  M.decodeMessage r & either (printWithContext "Message failed") (\m -> C.atomically $ sequence_ $ flip C.writeTQueue m <$> queues)
          C.race_ loop $ C.forConcurrently_ (zip acts (MessageQueue <$> queues)) \(act, queue) -> act initCoins queue
      badMessage -> putStrLn $ "Bad Init Message:" <> show badMessage

awaitMessage :: MessageQueue -> IO Message
awaitMessage (MessageQueue message) = C.atomically $ C.readTQueue message

sendTicker :: Server -> Coin -> (TM.TickerMessage, TM.TickerMessage) -> IO ()
sendTicker (Server send) coin messages =
  send $ MessageTicker coin messages

sendCoins :: Server -> [Coin] -> IO ()
sendCoins (Server send) coins =
  send $ MessageCoins coins

sendAlerts :: Server -> NonEmpty N.Alert -> IO ()
sendAlerts (Server send) alerts =
  send $ MessageAlerts alerts

withServer :: [Coin] -> (Server -> IO ()) -> App
withServer coins act transport = evalContT do
  context <- ContT Z.withContext
  pubSock <- ContT $ Z.withSocket context Z.Pub
  routerSock <- ContT $ Z.withSocket context Z.Router
  liftIO do
    latestCoinsRef <- Ref.newIORef coins
    Z.bind pubSock $ transportAddress transport DestinationMessage
    Z.bind routerSock $ transportAddress transport DestinationInit

    let send message = do
          case message of
            MessageCoins cs -> Ref.atomicWriteIORef latestCoinsRef cs
            _ -> pure ()
          Z.send pubSock [] $ M.encodeMessage message

    let routerLoop =
          forever $
            Z.receiveMulti routerSock >>= \case
              [identity, "", "init"] -> do
                cs <- Ref.readIORef latestCoinsRef
                let message = InitMessage cs
                Z.sendMulti routerSock (identity :| ["", M.encodeMessage message])
              badMessage -> putStrLn $ "Bad Router Message: " <> show badMessage

    C.race_ routerLoop (act $ Server send)
