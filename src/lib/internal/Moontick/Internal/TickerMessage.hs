module Moontick.Internal.TickerMessage
  ( TickerMessage (..),
  )
where

import Moontick.Internal.Message (IsMessage (..))
import qualified Moontick.Internal.Message as M

data TickerMessage = TickerMessage
  { _tickerId :: Int,
    _tickerPrice :: Double,
    _tickerPercent :: Double,
    _tickerTime :: Int
  }
  deriving (Show)

instance IsMessage TickerMessage where
  cborMessageEncoding TickerMessage {..} =
    M.encodeInt _tickerId
      <> M.encodeDouble _tickerPrice
      <> M.encodeDouble _tickerPercent
      <> M.encodeInt _tickerTime
  cborMessageDecoding = do
    tickerId <- M.decodeInt
    tickerPrice <- M.decodeDouble
    tickerPercent <- M.decodeDouble
    tickerTime <- M.decodeInt
    pure $
      TickerMessage
        { _tickerId = tickerId,
          _tickerPrice = tickerPrice,
          _tickerPercent = tickerPercent,
          _tickerTime = tickerTime
        }
