module Moontick.Internal.Message
  ( IsMessage (..),
    encodeMessage,
    decodeMessage,
    cborMessageEncodingList,
    cborMessageDecodingList,
    cborMessageEncodingMaybe,
    cborMessageDecodingMaybe,
    module E,
  )
where

import Codec.CBOR.Decoding as E
import qualified Codec.CBOR.Decoding as C
import Codec.CBOR.Encoding as E
import qualified Codec.CBOR.Encoding as C
import qualified Codec.CBOR.Read as C
import qualified Codec.CBOR.Write as C
import Data.ByteString (ByteString)
import Data.ByteString.Lazy (fromStrict)

class IsMessage a where
  cborMessageEncoding :: a -> C.Encoding
  cborMessageDecoding :: forall s. C.Decoder s a

cborMessageEncodingMaybe :: Maybe C.Encoding -> C.Encoding
cborMessageEncodingMaybe m = case m of
  Nothing -> C.encodeWord 0
  Just a -> C.encodeWord 1 <> a

cborMessageDecodingMaybe :: forall a s. C.Decoder s a -> C.Decoder s (Maybe a)
cborMessageDecodingMaybe valDec =
  C.decodeWord >>= \case
    0 -> pure Nothing
    1 -> Just <$> valDec
    badTag -> fail $ "Bad Tag for Maybe: " <> show badTag

decodeMessage :: forall a. IsMessage a => ByteString -> Either C.DeserialiseFailure a
decodeMessage str = snd <$> C.deserialiseFromBytes cborMessageDecoding (fromStrict str)

encodeMessage :: forall a. IsMessage a => a -> ByteString
encodeMessage a = C.toStrictByteString (cborMessageEncoding a)

cborMessageEncodingList :: forall a t. Foldable t => IsMessage a => t a -> C.Encoding
cborMessageEncodingList as = C.encodeListLenIndef <> foldMap cborMessageEncoding as <> C.encodeBreak

cborMessageDecodingList :: forall a s. IsMessage a => C.Decoder s [a]
cborMessageDecodingList = C.decodeListLenIndef *> C.decodeSequenceLenIndef (flip (:)) [] reverse cborMessageDecoding
