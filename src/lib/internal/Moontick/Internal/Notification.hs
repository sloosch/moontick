module Moontick.Internal.Notification
  ( Rule (..),
    Alert (..),
    isGreaterThan,
    isLowerThan,
    alertsIf,
    nextMatchingAlerts,
  )
where

import Moontick.Internal.Coin (Coin (..))
import Moontick.Internal.Message (IsMessage (..))
import qualified Moontick.Internal.Message as M

data Rule = GreaterThan Double | LowerThan Double deriving (Show)

isGreaterThan :: Double -> Rule
isGreaterThan = GreaterThan

isLowerThan :: Double -> Rule
isLowerThan = LowerThan

data Alert = Alert
  { _alertCoin :: Coin,
    _alertRule :: Rule,
    _alertSilent :: Bool
  }
  deriving (Show)

alertsIf :: Coin -> Rule -> Alert
alertsIf c r = Alert c r True

checkRuleAgainstRate :: Coin -> Double -> Alert -> Bool
checkRuleAgainstRate coin rate Alert {..} =
  coin == _alertCoin && not _alertSilent && case _alertRule of
    GreaterThan other -> rate > other
    LowerThan other -> rate < other

censorAlert :: Coin -> Double -> Alert -> Alert
censorAlert coin rate a@Alert {..}
  | coin == _alertCoin = case _alertRule of
    LowerThan lt -> a {_alertSilent = rate <= lt}
    GreaterThan gt -> a {_alertSilent = rate >= gt}
  | otherwise = a

nextMatchingAlerts :: [Alert] -> Coin -> Double -> ([Alert], [Alert])
nextMatchingAlerts alerts coin rate =
  let matchingAlerts = filter (checkRuleAgainstRate coin rate) alerts
      nextAlerts = censorAlert coin rate <$> alerts
   in (matchingAlerts, nextAlerts)

instance IsMessage Rule where
  cborMessageEncoding rule = case rule of
    GreaterThan val -> M.encodeWord 0 <> M.encodeDouble val
    LowerThan val -> M.encodeWord 1 <> M.encodeDouble val
  cborMessageDecoding =
    M.decodeWord >>= \case
      0 -> GreaterThan <$> M.decodeDouble
      1 -> LowerThan <$> M.decodeDouble
      badTag -> fail $ "Bad Tag for rule: " <> show badTag

instance IsMessage Alert where
  cborMessageEncoding Alert {..} =
    M.encodeBool _alertSilent
      <> cborMessageEncoding _alertCoin
      <> cborMessageEncoding _alertRule
  cborMessageDecoding = do
    silent <- M.decodeBool
    coin <- cborMessageDecoding
    rule <- cborMessageDecoding
    pure $
      Alert
        { _alertCoin = coin,
          _alertRule = rule,
          _alertSilent = silent
        }
