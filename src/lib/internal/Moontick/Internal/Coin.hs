module Moontick.Internal.Coin (Coin (..), findCoinById) where

import Data.Foldable (find)
import Data.Text (Text)
import Moontick.Internal.Message (IsMessage(..))
import qualified Moontick.Internal.Message as M

data Coin = Coin
  { _coinId :: Int,
    _coinName :: Text
  }
  deriving (Show)

instance Eq Coin where
  (==) (Coin idA _) (Coin idB _) = idA == idB

instance Ord Coin where
  compare (Coin idA _) (Coin idB _) = compare idA idB

findCoinById :: Int -> [Coin] -> Maybe Coin
findCoinById coinId = find ((==) coinId . _coinId)

instance IsMessage Coin where
  cborMessageEncoding Coin {..} = M.encodeInt _coinId <> M.encodeString _coinName
  cborMessageDecoding = Coin <$> M.decodeInt <*> M.decodeString
