module Moontick.Deamon.Control
  ( Action (..),
    Control,
    ActionResponse (..),
    withControl,
    withActions,
    sendAction,
  )
where

import Control.Monad (forever)
import Data.Text (Text)
import Formatting (fprintLn, string, (%))
import Moontick.Internal.Message (IsMessage (..))
import qualified Moontick.Internal.Message as M
import qualified Moontick.Internal.Notification as N
import qualified System.ZMQ4 as Z

data Action
  = SendAlert N.Alert
  | Mute
  | Unmute
  deriving (Show)

data ActionResponse = ActionResponseOK (Maybe Text) | ActionResponseNotUnderstood deriving (Show)

instance IsMessage Action where
  cborMessageEncoding m = case m of
    SendAlert alert -> M.encodeWord 0 <> cborMessageEncoding alert
    Mute -> M.encodeWord 1
    Unmute -> M.encodeWord 2
  cborMessageDecoding =
    M.decodeWord >>= \case
      0 -> SendAlert <$> cborMessageDecoding
      1 -> pure Mute
      2 -> pure Unmute
      badTag -> fail $ "Bad Action Type Tag:" <> show badTag

instance IsMessage ActionResponse where
  cborMessageEncoding m = case m of
    ActionResponseOK t -> M.encodeWord 0 <> M.cborMessageEncodingMaybe (M.encodeString <$> t)
    ActionResponseNotUnderstood -> M.encodeWord 1
  cborMessageDecoding =
    M.decodeWord >>= \case
      0 -> ActionResponseOK <$> M.cborMessageDecodingMaybe M.decodeString
      1 -> pure ActionResponseNotUnderstood
      badTag -> fail $ "Bad Tag for Action Response:" <> show badTag

newtype Control = Control (Action -> IO (Maybe ActionResponse))

sendAction :: Control -> Action -> IO (Maybe ActionResponse)
sendAction (Control send) = send

controlAddress :: String
controlAddress = "ipc:///tmp/moontickd_control"

withControl :: (Control -> IO ()) -> IO ()
withControl act =
  Z.withContext \context -> Z.withSocket context Z.Req \socket -> do
    Z.connect socket controlAddress
    let send action = do
          Z.send socket [] $ M.encodeMessage action
          r <- Z.receive socket
          case M.decodeMessage r of
            Left err -> Nothing <$ fprintLn ("Bad Control Action Response: " % string) (show err)
            Right resp -> pure $ Just resp
    act $ Control send

withActions :: (Action -> IO (Maybe Text)) -> IO ()
withActions act =
  Z.withContext \context -> Z.withSocket context Z.Rep \socket -> do
    Z.bind socket controlAddress
    let loop = forever do
          r <- Z.receive socket
          returnMsg <- case M.decodeMessage r of
            Left err -> ActionResponseNotUnderstood <$ fprintLn ("Bad Control Action: " % string) (show err)
            Right msg -> ActionResponseOK <$> act msg
          Z.send socket [] $ M.encodeMessage returnMsg
    loop