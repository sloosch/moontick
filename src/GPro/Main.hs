{-# LANGUAGE ApplicativeDo #-}

module Main where

import Control.Applicative ((<**>))
import Control.Monad (forever, join)
import Data.Char (toUpper)
import Data.Foldable (fold)
import Data.List (elemIndex)
import Data.List.NonEmpty (NonEmpty (..))
import qualified Data.Map as M
import Data.Maybe (fromMaybe)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Word (Word8)
import Formatting (fixed, fprintLn, sformat, stext, (%))
import Moontick.GPro.Animation (AnimationTimeline)
import qualified Moontick.GPro.Animation as A
import qualified Moontick.GPro.Device as GD
import qualified Moontick.GPro.DotMatrix as DM
import Moontick.Internal.Coin (Coin (..))
import qualified Moontick.Internal.MessageChannel as MC
import qualified Moontick.Internal.Notification as N
import qualified Options.Applicative as O

data Config = Config
  { _fgColor :: GD.RGB,
    _bgColor :: GD.RGB
  }

type KeyAnimation = AnimationTimeline [GD.ColorKey] GD.RGB

mixWithLighten :: [GD.ColorKey] -> [GD.ColorKey]
mixWithLighten keys =
  fmap (uncurry GD.ColorKey) $
    M.toList $
      M.fromListWith lighten $
        (\(GD.ColorKey k color) -> (k, color)) <$> keys
  where
    lighten :: GD.RGB -> GD.RGB -> GD.RGB
    lighten (GD.RGB r1 g1 b1) (GD.RGB r2 g2 b2) =
      GD.RGB (max r1 r2) (max g1 g2) (max b1 b2)

typerAnimation :: Config -> Text -> KeyAnimation
typerAnimation Config {..} msg =
  foldMap go (T.unpack $ T.toLower msg)
  where
    go c
      | c == ' ' = A.Delay 0.5
      | otherwise = fromMaybe mempty do
        coloring <- DM.coloringFromAlphaNum c
        pure $
          A.Sequence
            [ A.Run $ A.animation (DM.applyColorMaybe DM.keyMat4x5 $ coloring (Just _bgColor) Nothing) _fgColor 0.1,
              A.Delay 0.2,
              A.Run $ A.animation (DM.applyColorMaybe DM.keyMat4x5 $ coloring (Just _fgColor) Nothing) _bgColor 0.1
            ]

startupAnimation :: Config -> KeyAnimation
startupAnimation config@Config {..} =
  let toFg = A.Run $ A.animation (DM.applyColor DM.keyMat4x5 $ DM.fill _bgColor _bgColor) _fgColor 0.1
      toBg = A.Run $ A.animation (DM.applyColor DM.keyMat4x5 $ DM.fill _fgColor _bgColor) _bgColor 0.1
      blink = fold $ replicate 4 $ toFg <> toBg
      typer = typerAnimation config "moon"
   in blink <> typer <> blink <> toFg

runGProAnimation :: GD.GProDevice -> KeyAnimation -> IO ()
runGProAnimation gPro animation =
  GD.withOpenGPro gPro \openGPro ->
    A.runAnimationTimelineWith 24 animation (GD.writeKeys openGPro . mixWithLighten . join)

configParser :: O.Parser (Maybe MC.ChannelAddress, Config)
configParser = do
  address <- O.optional $ O.argument O.str (O.metavar "ADDRESS")
  fg <-
    O.option
      parseRGB
      ( O.long "foreground"
          <> O.short 'f'
          <> O.metavar "FOREGROUND"
          <> O.help "32bit hex color string e.g. 0xffee55"
      )
  bg <-
    O.option
      parseRGB
      ( O.long "background"
          <> O.short 'b'
          <> O.metavar "BACKGROUND"
          <> O.help "32bit hex color string e.g. 0xffee55"
      )
  pure (address, Config fg bg)
  where
    parseRGB :: O.ReadM GD.RGB
    parseRGB = O.eitherReader \s -> case hexStrToColor s of
      Nothing -> Left "Color not well defined as 32bit hex string"
      Just color -> Right color

    hexStrToColor :: String -> Maybe GD.RGB
    hexStrToColor ['0', 'x', ru, rl, gu, gl, bu, bl] =
      GD.RGB <$> twoHexCharToWord8 ru rl
        <*> twoHexCharToWord8 gu gl
        <*> twoHexCharToWord8 bu bl
    hexStrToColor _ = Nothing

    twoHexCharToWord8 :: Char -> Char -> Maybe Word8
    twoHexCharToWord8 upper lower =
      let hexes = "0123456789ABCDEF"
       in (+)
            <$> (fromIntegral . (*) 16 <$> elemIndex (toUpper upper) hexes)
            <*> (fromIntegral <$> elemIndex (toUpper lower) hexes)

waitForAlerts :: MC.MessageQueue -> IO (NonEmpty N.Alert)
waitForAlerts messages = go
  where
    go =
      MC.awaitMessage messages >>= \case
        MC.MessageAlerts alerts -> pure alerts
        _ -> go

gProLightSystem :: GD.GProDevice -> Config -> [Coin] -> MC.MessageQueue -> IO ()
gProLightSystem gPro Config {..} _ messages = forever go
  where
    go :: IO ()
    go = do
      (N.Alert {..} :| _) <- waitForAlerts messages
      let coinName = _coinName _alertCoin
          message = sformat (stext % " " % fixed 3) coinName case _alertRule of
            N.LowerThan lt -> lt
            N.GreaterThan gt -> gt
          fg = case _alertRule of
            N.LowerThan _ -> GD.RGB 0xff 0 0
            N.GreaterThan _ -> GD.RGB 0 0xff 0
          typer = typerAnimation (Config fg _bgColor) message
          animation =
            A.Hold (DM.applyColor DM.keyMat4x5 $ DM.fill _bgColor _bgColor) 0.1
              <> typer
              <> A.Delay 0.5
              <> typer
              <> A.Hold (DM.applyColor DM.keyMat4x5 $ DM.fill _fgColor _bgColor) 0.1
      runGProAnimation gPro animation

main :: IO ()
main = do
  (address, config) <- O.execParser (O.info (configParser <**> O.helper) O.fullDesc)
  GD.getGPro >>= \case
    Left err -> fprintLn stext err
    Right gPro -> do
      runGProAnimation gPro (startupAnimation config)
      MC.runApp address $ MC.withMessages [gProLightSystem gPro config]
