{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE UndecidableInstances #-}

module Moontick.GPro.Animation
  ( Animatable (..),
    AnimationTimeline (Run, Sequence, Parallel, Delay, Hold),
    animation,
    runAnimationTimelineWith,
    mapStartValue,
  )
where

import qualified Control.Concurrent as C
import Data.Word (Word8)

class Animatable s e | s -> e where
  interpolateBetween :: s -> e -> Float -> s

instance Animatable Word8 Word8 where
  interpolateBetween a b t = a + round ((fromIntegral b - fromIntegral a) * t)

instance (Animatable a e) => Animatable [a] e where
  interpolateBetween f e t = (\v -> interpolateBetween v e t) <$> f

data Animation s e = Animation
  { _animationStartValue :: s,
    _animationEndValue :: e,
    _animationDuration :: Float,
    _animationTime :: Float
  }

data AnimationTimeline s e
  = Run (Animation s e)
  | Sequence [AnimationTimeline s e]
  | Parallel [AnimationTimeline s e]
  | NoAnimation
  | Delay Float
  | Hold s Float

instance Semigroup (AnimationTimeline s e) where
  (<>) NoAnimation tl2 = tl2
  (<>) tl1 NoAnimation = tl1
  (<>) (Sequence seq1) (Sequence seq2) = Sequence $ seq1 <> seq2
  (<>) (Sequence seq1) tl2 = Sequence $ seq1 <> [tl2]
  (<>) tl1 tl2 = Sequence [tl1, tl2]

instance Monoid (AnimationTimeline s e) where
  mempty = NoAnimation

mapStartValue :: forall s ns e. (s -> ns) -> AnimationTimeline s e -> AnimationTimeline ns e
mapStartValue f timeline = case timeline of
  Run ani -> Run ani {_animationStartValue = f $ _animationStartValue ani}
  Sequence seqs -> Sequence $ mapStartValue f <$> seqs
  Parallel pars -> Parallel $ mapStartValue f <$> pars
  Delay n -> Delay n
  Hold v n -> Hold (f v) n
  NoAnimation -> NoAnimation

animation :: forall s e. Animatable s e => s -> e -> Float -> Animation s e
animation start end duration =
  Animation
    { _animationStartValue = start,
      _animationEndValue = end,
      _animationDuration = duration,
      _animationTime = 0
    }

stepAnimation :: forall s e. Animatable s e => Animation s e -> Float -> (s, Maybe (Animation s e))
stepAnimation a@Animation {..} step =
  let nextStep = min _animationDuration (_animationTime + step)
      normTime = _animationTime / _animationDuration
   in ( interpolateBetween _animationStartValue _animationEndValue normTime,
        if _animationTime >= _animationDuration then Nothing else Just a {_animationTime = nextStep}
      )

isNoAnimation :: forall s e. AnimationTimeline s e -> Bool
isNoAnimation = \case
  NoAnimation -> True
  _ -> False

stepAnimationTimeline :: forall s e. Animatable s e => AnimationTimeline s e -> Float -> ([s], AnimationTimeline s e)
stepAnimationTimeline (Run a) step =
  case stepAnimation a step of
    (val, next) -> (pure val, maybe NoAnimation Run next)
stepAnimationTimeline (Sequence seqs) step = case seqs of
  [] -> ([], NoAnimation)
  (a : as) -> case stepAnimationTimeline a step of
    (vals, NoAnimation) | null as -> (vals, NoAnimation)
    (vals, NoAnimation) -> (vals, Sequence as)
    (vals, other) -> (vals, Sequence (other : as))
stepAnimationTimeline (Parallel pars) step =
  let vs = flip stepAnimationTimeline step <$> pars
      vals = vs >>= fst
      nextPars = filter (not . isNoAnimation) $ snd <$> vs
   in (vals, if null nextPars then NoAnimation else Parallel nextPars)
stepAnimationTimeline NoAnimation _ = ([], NoAnimation)
stepAnimationTimeline (Delay n) step
  | n <= 0 = ([], NoAnimation)
  | otherwise = ([], Delay $ max 0 (n - step))
stepAnimationTimeline (Hold val n) step
  | n <= 0 = ([val], NoAnimation)
  | otherwise = ([val], Hold val $ max 0 (n - step))

runAnimationTimelineWith :: forall s e. Animatable s e => Float -> AnimationTimeline s e -> ([s] -> IO ()) -> IO ()
runAnimationTimelineWith fps timeline act = go timeline
  where
    delay = round $ 1000000 / fps
    stepSize = 1 / fps
    go ani =
      let (v, mnext) = stepAnimationTimeline ani stepSize
       in do
            act v
            case mnext of
              NoAnimation -> pure ()
              next -> C.threadDelay delay *> go next
