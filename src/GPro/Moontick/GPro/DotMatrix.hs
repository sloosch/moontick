module Moontick.GPro.DotMatrix
  ( Mat4x5,
    ColoringMat,
    keyMat4x5,
    applyColor,
    applyColorMaybe,
    fill,
    one,
    two,
    three,
    four,
    five,
    six,
    seven,
    eight,
    nine,
    zero,
    dot,
    alphaA,
    alphaB,
    alphaC,
    alphaD,
    alphaE,
    alphaF,
    alphaG,
    alphaH,
    alphaI,
    alphaJ,
    alphaK,
    alphaL,
    alphaM,
    alphaN,
    alphaO,
    alphaP,
    alphaQ,
    alphaR,
    alphaS,
    alphaT,
    alphaU,
    alphaV,
    alphaW,
    alphaX,
    alphaY,
    alphaZ,
    coloringFromAlphaNum,
  )
where

import Control.Monad (join)
import qualified Data.Map.Strict as M
import Data.Maybe (catMaybes)
import qualified Moontick.GPro.Device as GD
import Moontick.GPro.Key (AnyKey, Key (..), key)

newtype Mat4x5 a = Mat4x5 [[a]]

type ColoringMat c = c -> c -> Mat4x5 c

applyColor :: Mat4x5 AnyKey -> Mat4x5 GD.RGB -> [GD.ColorKey]
applyColor (Mat4x5 keyMat) (Mat4x5 colorMat) =
  join $ zipWith (zipWith GD.ColorKey) keyMat colorMat

applyColorMaybe :: Mat4x5 AnyKey -> Mat4x5 (Maybe GD.RGB) -> [GD.ColorKey]
applyColorMaybe (Mat4x5 keyMat) (Mat4x5 colorMat) =
  catMaybes $ join $ zipWith (zipWith (\k mc -> GD.ColorKey k <$> mc)) keyMat colorMat

keyMat4x5 :: Mat4x5 AnyKey
keyMat4x5 =
  Mat4x5
    [ [key @'F5, key @'F6, key @'F7, key @'F8],
      [key @'N6, key @'N7, key @'N8, key @'N9],
      [key @'T, key @'Y, key @'U, key @'I],
      [key @'F, key @'G, key @'H, key @'J],
      [key @'C, key @'V, key @'B, key @'N]
    ]

fill :: ColoringMat c
fill f _ =
  Mat4x5
    [ [f, f, f, f],
      [f, f, f, f],
      [f, f, f, f],
      [f, f, f, f],
      [f, f, f, f]
    ]

zero :: ColoringMat c
zero f b =
  Mat4x5
    [ [f, f, f, f],
      [f, b, b, f],
      [f, b, b, f],
      [f, b, b, f],
      [f, f, f, f]
    ]

one :: ColoringMat c
one f b =
  Mat4x5
    [ [b, b, f, f],
      [b, b, b, f],
      [b, b, b, f],
      [b, b, b, f],
      [b, b, b, f]
    ]

two :: ColoringMat c
two f b =
  Mat4x5
    [ [f, f, f, f],
      [b, b, b, f],
      [b, b, f, b],
      [b, f, b, b],
      [f, f, f, f]
    ]

three :: ColoringMat c
three f b =
  Mat4x5
    [ [f, f, f, f],
      [b, b, b, f],
      [f, f, f, f],
      [b, b, b, f],
      [f, f, f, f]
    ]

four :: ColoringMat c
four f b =
  Mat4x5
    [ [f, b, b, f],
      [f, b, b, f],
      [f, f, f, f],
      [b, b, b, f],
      [b, b, b, f]
    ]

five :: ColoringMat c
five f b =
  Mat4x5
    [ [f, f, f, f],
      [f, b, b, b],
      [f, f, f, f],
      [b, b, b, f],
      [f, f, f, f]
    ]

six :: ColoringMat c
six f b =
  Mat4x5
    [ [f, b, b, b],
      [f, b, b, b],
      [f, f, f, b],
      [f, b, b, f],
      [f, f, f, f]
    ]

seven :: ColoringMat c
seven f b =
  Mat4x5
    [ [f, f, f, f],
      [b, b, b, f],
      [b, b, b, f],
      [b, b, b, f],
      [b, b, b, f]
    ]

eight :: ColoringMat c
eight f b =
  Mat4x5
    [ [f, f, f, f],
      [f, b, b, f],
      [f, f, f, f],
      [f, b, b, f],
      [f, f, f, f]
    ]

nine :: ColoringMat c
nine f b =
  Mat4x5
    [ [f, f, f, f],
      [f, b, b, f],
      [b, f, f, f],
      [b, b, b, f],
      [b, b, b, f]
    ]

dot :: ColoringMat c
dot f b =
  Mat4x5
    [ [b, b, b, b],
      [b, b, b, b],
      [b, b, b, b],
      [b, f, f, b],
      [b, f, f, b]
    ]

alphaA :: ColoringMat c
alphaA f b =
  Mat4x5
    [ [b, f, f, b],
      [f, b, b, f],
      [f, f, f, f],
      [f, b, b, f],
      [f, b, b, f]
    ]

alphaB :: ColoringMat c
alphaB f b =
  Mat4x5
    [ [b, f, f, b],
      [f, b, b, f],
      [f, f, f, b],
      [f, b, b, f],
      [f, f, f, f]
    ]

alphaC :: ColoringMat c
alphaC f b =
  Mat4x5
    [ [b, f, f, f],
      [f, b, b, b],
      [f, b, b, b],
      [f, b, b, b],
      [b, f, f, f]
    ]

alphaD :: ColoringMat c
alphaD f b =
  Mat4x5
    [ [f, f, f, b],
      [f, b, b, f],
      [f, b, b, f],
      [f, b, b, f],
      [f, f, f, b]
    ]

alphaE :: ColoringMat c
alphaE f b =
  Mat4x5
    [ [f, f, f, f],
      [f, b, b, b],
      [f, f, f, f],
      [f, b, b, b],
      [f, f, f, f]
    ]

alphaF :: ColoringMat c
alphaF f b =
  Mat4x5
    [ [f, f, f, f],
      [f, b, b, b],
      [f, f, f, f],
      [f, b, b, b],
      [f, b, b, b]
    ]

alphaG :: ColoringMat c
alphaG f b =
  Mat4x5
    [ [b, f, f, f],
      [f, b, b, b],
      [f, b, f, f],
      [f, b, b, f],
      [f, f, f, f]
    ]

alphaH :: ColoringMat c
alphaH f b =
  Mat4x5
    [ [f, b, b, f],
      [f, b, b, f],
      [f, f, f, f],
      [f, b, b, f],
      [f, b, b, f]
    ]

alphaI :: ColoringMat c
alphaI f b =
  Mat4x5
    [ [b, b, f, b],
      [b, b, f, b],
      [b, b, f, b],
      [b, b, f, b],
      [b, b, f, b]
    ]

alphaJ :: ColoringMat c
alphaJ f b =
  Mat4x5
    [ [b, f, f, f],
      [b, b, b, f],
      [b, b, b, f],
      [b, b, b, f],
      [f, f, f, b]
    ]

alphaK :: ColoringMat c
alphaK f b =
  Mat4x5
    [ [f, b, b, f],
      [f, b, f, b],
      [f, f, b, b],
      [f, b, f, b],
      [f, b, b, f]
    ]

alphaL :: ColoringMat c
alphaL f b =
  Mat4x5
    [ [f, b, b, b],
      [f, b, b, b],
      [f, b, b, b],
      [f, b, b, b],
      [f, f, f, f]
    ]

alphaM :: ColoringMat c
alphaM f b =
  Mat4x5
    [ [f, b, b, f],
      [f, f, f, f],
      [f, b, b, f],
      [f, b, b, f],
      [f, b, b, f]
    ]

alphaN :: ColoringMat c
alphaN f b =
  Mat4x5
    [ [f, b, b, f],
      [f, f, b, f],
      [f, b, f, f],
      [f, b, f, f],
      [f, b, b, f]
    ]

alphaO :: ColoringMat c
alphaO f b =
  Mat4x5
    [ [b, f, f, b],
      [f, b, b, f],
      [f, b, b, f],
      [f, b, b, f],
      [b, f, f, b]
    ]

alphaP :: ColoringMat c
alphaP f b =
  Mat4x5
    [ [f, f, f, b],
      [f, b, b, f],
      [f, f, f, b],
      [f, b, b, b],
      [f, b, b, b]
    ]

alphaQ :: ColoringMat c
alphaQ f b =
  Mat4x5
    [ [b, f, f, b],
      [f, b, b, f],
      [f, b, b, f],
      [f, b, f, f],
      [b, f, f, f]
    ]

alphaR :: ColoringMat c
alphaR f b =
  Mat4x5
    [ [f, f, f, b],
      [f, b, b, f],
      [f, f, f, b],
      [f, b, f, b],
      [f, b, b, f]
    ]

alphaS :: ColoringMat c
alphaS f b =
  Mat4x5
    [ [f, f, f, b],
      [f, b, b, f],
      [b, f, f, b],
      [b, b, b, f],
      [f, f, f, b]
    ]

alphaT :: ColoringMat c
alphaT f b =
  Mat4x5
    [ [b, f, f, f],
      [b, b, f, b],
      [b, b, f, b],
      [b, b, f, b],
      [b, b, f, b]
    ]

alphaU :: ColoringMat c
alphaU f b =
  Mat4x5
    [ [f, b, b, f],
      [f, b, b, f],
      [f, b, b, f],
      [f, b, b, f],
      [b, f, f, b]
    ]

alphaV :: ColoringMat c
alphaV f b =
  Mat4x5
    [ [f, b, b, f],
      [f, b, b, f],
      [f, b, f, b],
      [f, f, b, b],
      [f, b, b, b]
    ]

alphaW :: ColoringMat c
alphaW f b =
  Mat4x5
    [ [f, b, b, f],
      [f, b, b, f],
      [f, b, b, f],
      [f, f, f, f],
      [f, b, b, f]
    ]

alphaX :: ColoringMat c
alphaX f b =
  Mat4x5
    [ [f, b, b, f],
      [f, b, b, f],
      [b, f, f, b],
      [b, f, f, b],
      [f, b, b, f]
    ]

alphaY :: ColoringMat c
alphaY f b =
  Mat4x5
    [ [f, b, b, f],
      [f, b, b, f],
      [b, f, f, b],
      [b, b, f, b],
      [b, b, f, b]
    ]

alphaZ :: ColoringMat c
alphaZ f b =
  Mat4x5
    [ [f, f, f, f],
      [b, b, b, f],
      [b, b, f, b],
      [b, f, b, b],
      [f, f, f, f]
    ]

alphaNumMapping :: M.Map Char (ColoringMat c)
alphaNumMapping =
  M.fromList $
    zip
      "abcdefghijklmnopqrstuvwxyz1234567890."
      [ alphaA,
        alphaB,
        alphaC,
        alphaD,
        alphaE,
        alphaF,
        alphaG,
        alphaH,
        alphaI,
        alphaJ,
        alphaK,
        alphaL,
        alphaM,
        alphaN,
        alphaO,
        alphaP,
        alphaQ,
        alphaR,
        alphaS,
        alphaT,
        alphaU,
        alphaV,
        alphaW,
        alphaX,
        alphaY,
        alphaZ,
        one,
        two,
        three,
        four,
        five,
        six,
        seven,
        eight,
        nine,
        zero,
        dot
      ]

coloringFromAlphaNum :: Char -> Maybe (ColoringMat c)
coloringFromAlphaNum c = M.lookup c alphaNumMapping