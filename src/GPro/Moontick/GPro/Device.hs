module Moontick.GPro.Device
  ( GProDevice,
    OpenGProDevice,
    getGPro,
    ColorKey (..),
    RGB (..),
    withOpenGPro,
    writeKeys,
  )
where

import qualified Control.Concurrent as C
import Control.Exception (bracket)
import Control.Monad (void, (>=>))
import Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Unsafe as BS
import Data.Foldable (traverse_)
import Data.List.Split (chunksOf)
import qualified Data.Map.Strict as M
import Data.Maybe (catMaybes)
import Data.Text (Text)
import qualified Data.Text as Text
import Data.Text.Encoding (decodeUtf8)
import Data.Word (Word8)
import qualified Foreign as F
import Moontick.GPro.Animation (Animatable (..))
import Moontick.GPro.Key (AnyKey, KeyGroup, keyCodeAndGroup, keyRegisterCode)
import qualified System.Posix as PIO
import qualified System.UDev as UDev

newtype GProDevice = GProDevice String deriving (Show)

newtype OpenGProDevice = OpenGProDevice PIO.Fd

logitechVendorId :: ByteString
logitechVendorId = "046d"

gProProductId :: ByteString
gProProductId = "c339"

largeFrame :: ByteString
largeFrame = BS.pack [0x12]

smallFrame :: ByteString
smallFrame = BS.pack [0x11]

storeOp :: ByteString
storeOp = BS.pack [0xff, 0x0c, 0x3b]

commitOp :: ByteString
commitOp = BS.pack [0xff, 0x0c, 0x5b]

data RGB = RGB Word8 Word8 Word8

data ColorKey = ColorKey AnyKey RGB

pad0Right :: Int -> ByteString -> ByteString
pad0Right size bs = BS.take size $ bs <> BS.pack (replicate size 0x0)

storeKeysFrames :: [ColorKey] -> [ByteString]
storeKeysFrames keys =
  let keyCodeAndColorGroupedByKeyGroup =
        M.fromListWith (<>) $
          (\(ColorKey key color) -> let (code, keyGroup) = keyCodeAndGroup key in (keyGroup, [(code, color)])) <$> keys
   in M.foldMapWithKey toFrames keyCodeAndColorGroupedByKeyGroup
  where
    toFrames :: KeyGroup -> [(Word8, RGB)] -> [ByteString]
    toFrames group keyCodeAndColor = do
      keysForFrame <- chunksOf maxNumberOfKeysForLargeFrame keyCodeAndColor
      let numberOfKeys = length keysForFrame
      let encodedKeys = foldMap keyCodeAndColorToBs keysForFrame
      let payload = storeOp <> keyStoreHeader group numberOfKeys <> encodedKeys
      pure $
        if numberOfKeys <= maxNumberOfKeysForSmallFrame
          then pad0Right 20 $ smallFrame <> payload
          else pad0Right 64 $ largeFrame <> payload

    maxNumberOfKeysForSmallFrame :: Int
    maxNumberOfKeysForSmallFrame = 3 -- 20 byte - 8 byte preamble, 4 byte a key
    maxNumberOfKeysForLargeFrame :: Int
    maxNumberOfKeysForLargeFrame = 14 -- 64 byte - 8 byte preamble, 4 byte a key
    keyStoreHeader :: KeyGroup -> Int -> ByteString
    keyStoreHeader keyGroup numberOfKeys =
      BS.pack [0x00, keyRegisterCode keyGroup, 0x00, fromIntegral numberOfKeys]
    keyCodeAndColorToBs :: (Word8, RGB) -> ByteString
    keyCodeAndColorToBs (keyCode, RGB r g b) =
      BS.pack [keyCode, r, g, b]

instance Animatable RGB RGB where
  interpolateBetween (RGB sr sg sb) (RGB er eg eb) t =
    RGB (interpolateBetween sr er t) (interpolateBetween sg eg t) (interpolateBetween sb eb t)

instance Animatable ColorKey RGB where
  interpolateBetween (ColorKey code color1) color2 =
    ColorKey code . interpolateBetween color1 color2

getGPro :: IO (Either Text GProDevice)
getGPro =
  UDev.withUDev $ \udev -> do
    e <- UDev.newEnumerate udev
    UDev.addMatchSubsystem e "hidraw"
    UDev.scanDevices e
    UDev.getListEntry e >>= \case
      Nothing -> pure $ Left "No hidraw device found"
      Just devList -> do
        listOfGPros <- catMaybes <$> traverseUDevList (UDev.getName >=> getGProFromPath udev) devList
        case listOfGPros of
          [] -> pure $ Left "GPro not found"
          (firstGPro : _) -> pure $ Right firstGPro
  where
    traverseUDevList :: forall a. (UDev.List -> IO a) -> UDev.List -> IO [a]
    traverseUDevList act = go []
      where
        go res list = do
          r <- act list
          UDev.getNext list >>= \case
            Nothing -> pure (r : res)
            Just nextList -> go (r : res) nextList

    getGProFromPath :: UDev.UDev -> ByteString -> IO (Maybe GProDevice)
    getGProFromPath udev path = do
      dev <- UDev.newFromSysPath udev path
      UDev.getParentWithSubsystemDevtype dev "usb" "usb_device" >>= \case
        Nothing -> pure Nothing
        Just usbDev -> do
          vid <- UDev.getSysattrValue usbDev "idVendor"
          pid <- UDev.getSysattrValue usbDev "idProduct"
          pure
            if vid == logitechVendorId && pid == gProProductId
              then GProDevice . Text.unpack . decodeUtf8 <$> UDev.getDevnode dev
              else Nothing

withOpenGPro :: forall a. GProDevice -> (OpenGProDevice -> IO a) -> IO a
withOpenGPro (GProDevice path) act =
  bracket (PIO.openFd path PIO.WriteOnly Nothing PIO.defaultFileFlags) PIO.closeFd (act . OpenGProDevice)

writeKeys :: OpenGProDevice -> [ColorKey] -> IO ()
writeKeys (OpenGProDevice fd) keys
  | null keys = pure ()
  | otherwise =
    let commit = pad0Right 20 $ smallFrame <> commitOp
        frames = storeKeysFrames keys <> [commit]
     in traverse_ (\bs -> writeBs bs *> C.threadDelay 500) frames
  where
    writeBs :: ByteString -> IO ()
    writeBs bs =
      void $ BS.unsafeUseAsCStringLen bs \(ptr, len) ->
        PIO.fdWriteBuf fd (F.castPtr ptr) (fromIntegral len)
