{-# LANGUAGE ExistentialQuantification #-}

module Moontick.GPro.Key
  ( Key (..),
    key,
    AnyKey,
    KeyGroup,
    keyCodeAndGroup,
    keyRegisterCode,
  )
where

import Data.Data (Proxy (..))
import Data.Word (Word8)

data KeyGroup = Logo | Indicator | SomeKey deriving (Eq, Ord)

data Key
  = A
  | B
  | C
  | D
  | E
  | F
  | G
  | H
  | I
  | J
  | K
  | L
  | M
  | N
  | O
  | P
  | Q
  | R
  | S
  | T
  | U
  | V
  | W
  | X
  | Y
  | Z
  | N1
  | N2
  | N3
  | N4
  | N5
  | N6
  | N7
  | N8
  | N9
  | N0
  | Enter
  | Esc
  | Backspace
  | Tab
  | Space
  | Minus
  | Equal
  | OpenBracket
  | CloseBracket
  | Backslash
  | Dollar
  | Semicolon
  | Quote
  | Tilde
  | Comma
  | Period
  | Slash
  | CapsLock
  | F1
  | F2
  | F3
  | F4
  | F5
  | F6
  | F7
  | F8
  | F9
  | F10
  | F11
  | F12
  | PrintScreen
  | ScrollLock
  | PauseBreak
  | Insert
  | Home
  | PageUp
  | Del
  | End
  | PageDown
  | ArrowRight
  | ArrowLeft
  | ArrowDown
  | ArrowUp
  | IntlBackslash
  | Menu
  | ControlLeft
  | ShiftLeft
  | AltLeft
  | Windows
  | ControlRight
  | ShiftRight
  | AltRight
  | Fn
  | Backlight
  | GameMode
  | CapsLockIndicator
  | ScrollLockIndicator
  | LogoLight

class IsKey (k :: Key) where
  keyCode :: Word8
  keyGroup :: KeyGroup

instance IsKey 'A where
  keyCode = 0x04
  keyGroup = SomeKey

instance IsKey 'B where
  keyCode = 0x05
  keyGroup = SomeKey

instance IsKey 'C where
  keyCode = 0x06
  keyGroup = SomeKey

instance IsKey 'D where
  keyCode = 0x07
  keyGroup = SomeKey

instance IsKey 'E where
  keyCode = 0x08
  keyGroup = SomeKey

instance IsKey 'F where
  keyCode = 0x09
  keyGroup = SomeKey

instance IsKey 'G where
  keyCode = 0x0a
  keyGroup = SomeKey

instance IsKey 'H where
  keyCode = 0x0b
  keyGroup = SomeKey

instance IsKey 'I where
  keyCode = 0x0c
  keyGroup = SomeKey

instance IsKey 'J where
  keyCode = 0x0d
  keyGroup = SomeKey

instance IsKey 'K where
  keyCode = 0x0e
  keyGroup = SomeKey

instance IsKey 'L where
  keyCode = 0x0f
  keyGroup = SomeKey

instance IsKey 'M where
  keyCode = 0x10
  keyGroup = SomeKey

instance IsKey 'N where
  keyCode = 0x11
  keyGroup = SomeKey

instance IsKey 'O where
  keyCode = 0x12
  keyGroup = SomeKey

instance IsKey 'P where
  keyCode = 0x13
  keyGroup = SomeKey

instance IsKey 'Q where
  keyCode = 0x14
  keyGroup = SomeKey

instance IsKey 'R where
  keyCode = 0x15
  keyGroup = SomeKey

instance IsKey 'S where
  keyCode = 0x16
  keyGroup = SomeKey

instance IsKey 'T where
  keyCode = 0x17
  keyGroup = SomeKey

instance IsKey 'U where
  keyCode = 0x18
  keyGroup = SomeKey

instance IsKey 'V where
  keyCode = 0x19
  keyGroup = SomeKey

instance IsKey 'W where
  keyCode = 0x1a
  keyGroup = SomeKey

instance IsKey 'X where
  keyCode = 0x1b
  keyGroup = SomeKey

instance IsKey 'Y where
  keyCode = 0x1c
  keyGroup = SomeKey

instance IsKey 'Z where
  keyCode = 0x1d
  keyGroup = SomeKey

instance IsKey 'N1 where
  keyCode = 0x1e
  keyGroup = SomeKey

instance IsKey 'N2 where
  keyCode = 0x1f
  keyGroup = SomeKey

instance IsKey 'N3 where
  keyCode = 0x20
  keyGroup = SomeKey

instance IsKey 'N4 where
  keyCode = 0x21
  keyGroup = SomeKey

instance IsKey 'N5 where
  keyCode = 0x22
  keyGroup = SomeKey

instance IsKey 'N6 where
  keyCode = 0x23
  keyGroup = SomeKey

instance IsKey 'N7 where
  keyCode = 0x24
  keyGroup = SomeKey

instance IsKey 'N8 where
  keyCode = 0x25
  keyGroup = SomeKey

instance IsKey 'N9 where
  keyCode = 0x26
  keyGroup = SomeKey

instance IsKey 'N0 where
  keyCode = 0x27
  keyGroup = SomeKey

instance IsKey 'Enter where
  keyCode = 0x28
  keyGroup = SomeKey

instance IsKey 'Esc where
  keyCode = 0x29
  keyGroup = SomeKey

instance IsKey 'Backspace where
  keyCode = 0x2a
  keyGroup = SomeKey

instance IsKey 'Tab where
  keyCode = 0x2b
  keyGroup = SomeKey

instance IsKey 'Space where
  keyCode = 0x2c
  keyGroup = SomeKey

instance IsKey 'Minus where
  keyCode = 0x2d
  keyGroup = SomeKey

instance IsKey 'Equal where
  keyCode = 0x2e
  keyGroup = SomeKey

instance IsKey 'OpenBracket where
  keyCode = 0x2f
  keyGroup = SomeKey

instance IsKey 'CloseBracket where
  keyCode = 0x30
  keyGroup = SomeKey

instance IsKey 'Backslash where
  keyCode = 0x31
  keyGroup = SomeKey

instance IsKey 'Dollar where
  keyCode = 0x32
  keyGroup = SomeKey

instance IsKey 'Semicolon where
  keyCode = 0x33
  keyGroup = SomeKey

instance IsKey 'Quote where
  keyCode = 0x34
  keyGroup = SomeKey

instance IsKey 'Tilde where
  keyCode = 0x35
  keyGroup = SomeKey

instance IsKey 'Comma where
  keyCode = 0x36
  keyGroup = SomeKey

instance IsKey 'Period where
  keyCode = 0x37
  keyGroup = SomeKey

instance IsKey 'Slash where
  keyCode = 0x38
  keyGroup = SomeKey

instance IsKey 'CapsLock where
  keyCode = 0x39
  keyGroup = SomeKey

instance IsKey 'F1 where
  keyCode = 0x3a
  keyGroup = SomeKey

instance IsKey 'F2 where
  keyCode = 0x3b
  keyGroup = SomeKey

instance IsKey 'F3 where
  keyCode = 0x3c
  keyGroup = SomeKey

instance IsKey 'F4 where
  keyCode = 0x3d
  keyGroup = SomeKey

instance IsKey 'F5 where
  keyCode = 0x3e
  keyGroup = SomeKey

instance IsKey 'F6 where
  keyCode = 0x3f
  keyGroup = SomeKey

instance IsKey 'F7 where
  keyCode = 0x40
  keyGroup = SomeKey

instance IsKey 'F8 where
  keyCode = 0x41
  keyGroup = SomeKey

instance IsKey 'F9 where
  keyCode = 0x42
  keyGroup = SomeKey

instance IsKey 'F10 where
  keyCode = 0x43
  keyGroup = SomeKey

instance IsKey 'F11 where
  keyCode = 0x44
  keyGroup = SomeKey

instance IsKey 'F12 where
  keyCode = 0x45
  keyGroup = SomeKey

instance IsKey 'PrintScreen where
  keyCode = 0x46
  keyGroup = SomeKey

instance IsKey 'ScrollLock where
  keyCode = 0x47
  keyGroup = SomeKey

instance IsKey 'PauseBreak where
  keyCode = 0x48
  keyGroup = SomeKey

instance IsKey 'Insert where
  keyCode = 0x49
  keyGroup = SomeKey

instance IsKey 'Home where
  keyCode = 0x4a
  keyGroup = SomeKey

instance IsKey 'PageUp where
  keyCode = 0x4b
  keyGroup = SomeKey

instance IsKey 'Del where
  keyCode = 0x4c
  keyGroup = SomeKey

instance IsKey 'End where
  keyCode = 0x4d
  keyGroup = SomeKey

instance IsKey 'PageDown where
  keyCode = 0x4e
  keyGroup = SomeKey

instance IsKey 'ArrowRight where
  keyCode = 0x4f
  keyGroup = SomeKey

instance IsKey 'ArrowLeft where
  keyCode = 0x50
  keyGroup = SomeKey

instance IsKey 'ArrowDown where
  keyCode = 0x51
  keyGroup = SomeKey

instance IsKey 'ArrowUp where
  keyCode = 0x52
  keyGroup = SomeKey

instance IsKey 'IntlBackslash where
  keyCode = 0x64
  keyGroup = SomeKey

instance IsKey 'Menu where
  keyCode = 0x65
  keyGroup = SomeKey

instance IsKey 'ControlLeft where
  keyCode = 0xe0
  keyGroup = SomeKey

instance IsKey 'ShiftLeft where
  keyCode = 0xe1
  keyGroup = SomeKey

instance IsKey 'AltLeft where
  keyCode = 0xe2
  keyGroup = SomeKey

instance IsKey 'Windows where
  keyCode = 0xe3
  keyGroup = SomeKey

instance IsKey 'ControlRight where
  keyCode = 0xe4
  keyGroup = SomeKey

instance IsKey 'ShiftRight where
  keyCode = 0xe5
  keyGroup = SomeKey

instance IsKey 'AltRight where
  keyCode = 0xe6
  keyGroup = SomeKey

instance IsKey 'Fn where
  keyCode = 0xe7
  keyGroup = SomeKey

instance IsKey 'Backlight where
  keyCode = 0x01
  keyGroup = Indicator

instance IsKey 'GameMode where
  keyCode = 0x02
  keyGroup = Indicator

instance IsKey 'CapsLockIndicator where
  keyCode = 0x03
  keyGroup = Indicator

instance IsKey 'ScrollLockIndicator where
  keyCode = 0x04
  keyGroup = Indicator

instance IsKey 'LogoLight where
  keyCode = 0x01
  keyGroup = Logo

data AnyKey = forall k. IsKey k => AnyKey (Proxy k)

instance Eq AnyKey where
  (==) keyA keyB =
    fst (keyCodeAndGroup keyA) == fst (keyCodeAndGroup keyB)

instance Ord AnyKey where
  compare keyA keyB =
    compare (fst $ keyCodeAndGroup keyA) (fst $ keyCodeAndGroup keyB)

key :: forall k. IsKey k => AnyKey
key = AnyKey (Proxy :: Proxy k)

keyCodeAndGroup :: AnyKey -> (Word8, KeyGroup)
keyCodeAndGroup (AnyKey w) = go w
  where
    go :: forall k. IsKey k => Proxy k -> (Word8, KeyGroup)
    go _ = (keyCode @k, keyGroup @k)

keyRegisterCode :: KeyGroup -> Word8
keyRegisterCode = \case
  SomeKey -> 0x01
  Logo -> 0x10
  Indicator -> 0x40