module Main where

import Control.Applicative ((<|>))
import qualified Control.Concurrent as C
import qualified Control.Concurrent.Async as C
import Control.Monad (void, when)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.Maybe (MaybeT (..), runMaybeT)
import qualified Data.IORef as Ref
import Data.List ((\\))
import qualified Data.List.NonEmpty as NEL
import Data.Text (Text)
import Formatting (fprintLn, stext, string, (%))
import qualified Moontick.Deamon.Config as Config
import qualified Moontick.Deamon.Control as Ctrl
import qualified Moontick.Deamon.TickerStream as TS
import Moontick.Internal.Coin (Coin)
import qualified Moontick.Internal.MessageChannel as MC
import qualified Moontick.Internal.Notification as N
import qualified Moontick.Internal.TickerMessage as TM

data AppState = AppState {_stateLatestAlerts :: [N.Alert], _stateMute :: Bool}

newtype AppStateRef = AppStateRef (Ref.IORef AppState)

emptyAppState :: [N.Alert] -> AppState
emptyAppState alerts = AppState {_stateLatestAlerts = alerts, _stateMute = False}

newEmptyAppState :: [N.Alert] -> IO AppStateRef
newEmptyAppState alerts = AppStateRef <$> Ref.newIORef (emptyAppState alerts)

modifyAppStateRef :: AppStateRef -> (AppState -> AppState) -> IO ()
modifyAppStateRef (AppStateRef ref) f = Ref.atomicModifyIORef' ref \state -> (f state, ())

readAppState :: AppStateRef -> IO AppState
readAppState (AppStateRef ref) = Ref.readIORef ref

main :: IO ()
main = do
  configFilePath <- Config.getConfigFilePath
  fprintLn ("Using '" % string % "' config file") configFilePath
  (initialCoins, initialAlerts) <- Config.parseCoinsAndAlerts configFilePath
  MC.runCLIApp $ MC.withServer initialCoins \server -> do
    latestAppState <- newEmptyAppState initialAlerts
    coinsVar <- C.newMVar initialCoins

    let watchConfigChanges prevCoins configRes = case configRes of
          Left err -> do
            fprintLn ("Config error: " % stext) err
            pure prevCoins
          Right (nextCoins, nextAlerts) -> do
            putStrLn "Config changed..."
            modifyAppStateRef latestAppState \state -> state {_stateLatestAlerts = nextAlerts}
            when (hasDifferentCoins prevCoins nextCoins) do
              C.putMVar coinsVar nextCoins
              MC.sendCoins server nextCoins
            pure nextCoins

    let sendAlertsMuteProtected alerts = do
          AppState {..} <- readAppState latestAppState
          if _stateMute
            then pure $ Just ("Did not send an alert, deamon is muted" :: Text)
            else Nothing <$ MC.sendAlerts server alerts

    let handleControlAction action = case action of
          Ctrl.SendAlert alert -> (\msg -> msg <|> Just "Alert sent") <$> sendAlertsMuteProtected (pure alert)
          Ctrl.Mute -> do
            putStrLn "Muting Alerts"
            Just "Alerts muted" <$ modifyAppStateRef latestAppState \state -> state {_stateMute = True}
          Ctrl.Unmute -> do
            putStrLn "Unmuting Alerts"
            Just "Alerts unmuted" <$ modifyAppStateRef latestAppState \state -> state {_stateMute = False}

    let sendAlertForTickerMessage coin tickerMessage = do
          AppState {..} <- readAppState latestAppState
          let (matchingAlerts, nextAlerts) = N.nextMatchingAlerts _stateLatestAlerts coin (TM._tickerPrice tickerMessage)
          modifyAppStateRef latestAppState \state -> state {_stateLatestAlerts = nextAlerts}
          void $ runMaybeT do
            alertsToSend <- MaybeT $ pure $ NEL.nonEmpty matchingAlerts
            msg <- MaybeT $ sendAlertsMuteProtected alertsToSend
            liftIO $ fprintLn stext msg

    let handleTickerMessage coin messages@(latest, _) = do
          print latest
          MC.sendTicker server coin messages
          sendAlertForTickerMessage coin latest

    void $
      C.runConcurrently $
        C.Concurrently
          (TS.withCoinRates (C.takeMVar coinsVar) handleTickerMessage)
          <|> C.Concurrently (void $ Config.foldWithConfigChanges configFilePath watchConfigChanges initialCoins)
          <|> C.Concurrently (Ctrl.withActions handleControlAction)
  where
    hasDifferentCoins :: [Coin] -> [Coin] -> Bool
    hasDifferentCoins coins newCoins =
      let added = newCoins \\ coins
          removed = coins \\ newCoins
       in not (null added) || not (null removed)
