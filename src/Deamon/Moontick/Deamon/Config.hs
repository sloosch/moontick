{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TypeApplications #-}

module Moontick.Deamon.Config
  ( parseCoinsAndAlerts,
    getConfigFilePath,
    ConfigFilePath,
    foldWithConfigChanges,
  )
where

import Control.Applicative ((<|>))
import qualified Control.Concurrent as C
import Control.Exception (SomeException, try)
import Data.Bifunctor (first)
import Data.Maybe (fromMaybe)
import Data.Text (Text, pack)
import Data.Text.Encoding (encodeUtf8)
import Dhall (FromDhall, Generic, Natural, auto, inputFile)
import qualified Moontick.Internal.Coin as C
import qualified Moontick.Internal.Notification as N
import qualified System.Directory as D
import qualified System.INotify as I
import Prelude hiding (id)

type ConfigFilePath = FilePath

data ConfigAlertValueGen
  = Points [Double]
  | LinearSteps {from :: Double, to :: Double, step :: Double}
  | Fibonacci {from :: Double, to :: Double, ratios :: Natural}
  deriving (Generic)

instance FromDhall ConfigAlertValueGen

data ConfigAlertRule
  = GreaterThan ConfigAlertValueGen
  | LowerThan ConfigAlertValueGen
  deriving (Generic)

instance FromDhall ConfigAlertRule

data ConfigCoin = ConfigCoin {id :: Natural, name :: Text, alerts :: [ConfigAlertRule]} deriving (Generic)

instance FromDhall ConfigCoin

newtype Config = Config {coins :: [ConfigCoin]} deriving (Generic)

instance FromDhall Config

configFileName :: String
configFileName = "moontick.dhall"

getConfigFilePath :: IO ConfigFilePath
getConfigFilePath = do
  configHomePath <- D.getXdgDirectory D.XdgConfig "" >>= ensureFileExistsInDirectory configFileName
  homePath <- D.getHomeDirectory >>= ensureFileExistsInDirectory hiddenConfigFileName
  let pwdPath = "./" <> hiddenConfigFileName

  pure $ fromMaybe pwdPath (configHomePath <|> homePath)
  where
    hiddenConfigFileName = "." <> configFileName
    ensureFileExistsInDirectory file dir =
      let path = dir <> "/" <> file
       in do
            exist <- D.doesFileExist path
            pure $ if exist then Just path else Nothing

parseConfig :: ConfigFilePath -> IO Config
parseConfig = inputFile auto

toRule :: ConfigAlertRule -> [N.Rule]
toRule rule = case rule of
  GreaterThan valueGen -> N.isGreaterThan <$> genValues valueGen
  LowerThan valueGen -> N.isLowerThan <$> genValues valueGen
  where
    genValues :: ConfigAlertValueGen -> [Double]
    genValues = \case
      Points vals -> vals
      LinearSteps {..} ->
        let from' = min from to
            to' = max from to
         in enumFromThenTo from' (from' + step) to'
      Fibonacci {..} ->
        let from' = min from to
            to' = max from to
            range = to' - from'
         in fmap (\n -> from' + n * range) $
              [0, 0.5, 1] <> do
                ns <- take (fromIntegral ratios) $ drop 1 fibRatios
                let n = ns !! 100 -- choose 100th fib number for precision
                let r = 1 - n
                [n, r]
    fibonaccis :: [Integer]
    fibonaccis = fib <$> [0 ..]
      where
        fib :: Integer -> Integer
        fib n = round $ phi ** fromIntegral n / sq5
        sq5 = sqrt 5 :: Double
        phi = (1 + sq5) / 2
    fibRatios :: [[Double]]
    fibRatios = [zipWith (\a b -> fromIntegral a / fromIntegral b) fibonaccis (drop i fibonaccis) | i <- [1 ..]]

toCoinAndAlerts :: ConfigCoin -> (C.Coin, [N.Alert])
toCoinAndAlerts ConfigCoin {..} =
  let coin = C.Coin (fromIntegral id) name
      rules = alerts >>= toRule
      nAlerts = N.alertsIf coin <$> rules
   in (coin, nAlerts)

parseCoinsAndAlerts :: ConfigFilePath -> IO ([C.Coin], [N.Alert])
parseCoinsAndAlerts path = getCoinsAndAlerts <$> parseConfig path
  where
    getCoinsAndAlerts Config {..} =
      let allCoinsAndAlerts = toCoinAndAlerts <$> coins
       in (fst <$> allCoinsAndAlerts, allCoinsAndAlerts >>= snd)

foldWithConfigChanges :: forall a. ConfigFilePath -> (a -> Either Text ([C.Coin], [N.Alert]) -> IO a) -> a -> IO a
foldWithConfigChanges path act initial = do
  inotify <- I.initINotify
  m <- C.newEmptyMVar
  let go a = do
        _ <- I.addWatch inotify [I.Modify] (encodeUtf8 $ pack path) $ const $ C.putMVar m ()
        C.takeMVar m
        parseResult <- try @SomeException (parseCoinsAndAlerts path)
        act a (first (pack . show) parseResult) >>= go
  go initial