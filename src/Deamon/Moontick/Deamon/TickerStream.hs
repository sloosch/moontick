{-# LANGUAGE TypeApplications #-}

module Moontick.Deamon.TickerStream (withCoinRates) where

import Control.Applicative ((<|>))
import qualified Control.Concurrent as C
import qualified Control.Concurrent.Async as C
import qualified Control.Concurrent.Thread.Delay as CT
import Control.Exception (handle)
import Control.Monad (forever)
import Data.Aeson (FromJSON (..), ToJSON (..), eitherDecodeStrict', encode, object, withObject, (.:), (.=))
import Data.ByteString (ByteString)
import Data.Function ((&))
import qualified Data.IORef as Ref
import qualified Data.Map.Strict as M
import Data.Text (Text, intercalate, pack)
import Formatting (int, stext, string, (%))
import Formatting.Internal (fprintLn)
import Moontick.Internal.Coin (Coin (..), findCoinById)
import qualified Moontick.Internal.TickerMessage as TM
import qualified Network.WebSockets as W
import qualified Wuss as W

newtype PriceSubscriptionMessage = PriceSubscriptionMessage [Int]

instance ToJSON PriceSubscriptionMessage where
  toJSON (PriceSubscriptionMessage ids) =
    object
      [ "method" .= ("subscribe" :: Text),
        "id" .= ("price" :: Text),
        "data"
          .= object
            [ "cryptoIds" .= ids
            ]
      ]

data PriceUnsuscribeMessage = PriceUnsubscribeMessage

instance ToJSON PriceUnsuscribeMessage where
  toJSON _ =
    object
      [ "method" .= ("unsubscribe" :: Text),
        "id" .= ("unsubscribePrice" :: Text)
      ]

newtype TickerStreamMessage = TickerStreamMessage TM.TickerMessage deriving (Show)

data TickerWrapperMessage
  = TickerPrice TickerStreamMessage
  | TickerUnsubscribe
  | TickerUnknown Text
  deriving (Show)

instance FromJSON TickerStreamMessage where
  parseJSON = withObject "TickerStreamMessage" \v -> do
    cr <- v .: "d" >>= (.: "cr")
    tickerId <- cr .: "id"
    tickerPrice <- cr .: "p"
    tickerPercent <- cr .: "p24h"
    tickerTime <- v .: "d" >>= (.: "t")

    pure $
      TickerStreamMessage $
        TM.TickerMessage
          { _tickerId = tickerId,
            _tickerPrice = tickerPrice,
            _tickerPercent = tickerPercent,
            _tickerTime = tickerTime
          }

instance FromJSON TickerWrapperMessage where
  parseJSON o =
    o & withObject "TickerWrapperMessage" \v ->
      (v .: "id") >>= \case
        "price" -> TickerPrice <$> parseJSON o
        "unsubscribePrice" -> pure TickerUnsubscribe
        other -> pure $ TickerUnknown other

data WsResult = WsError Text | WsDone

data WsReceiverMessage
  = WsRecvAbort
  | WsRecvMessage ByteString
  | WsRecvTimeout Text
  deriving (Show)

type Receiver = W.Connection -> IO WsReceiverMessage

receiverWithCoins :: IO [Coin] -> Receiver
receiverWithCoins waitForCoins connection =
  C.runConcurrently $
    (WsRecvAbort <$ C.Concurrently waitForCoins)
      <|> (WsRecvMessage <$> C.Concurrently (W.receiveData connection))
      <|> (WsRecvTimeout "Did not receive anything for 10s" <$ C.Concurrently (CT.delay 10000000))

ws :: [Coin] -> Receiver -> (Coin -> (TM.TickerMessage, TM.TickerMessage) -> IO ()) -> W.ClientApp WsResult
ws initialCoins receiver foldCoinWithRate connection = do
  putStrLn "Connected"

  let subscribeToCoins coins = do
        W.sendTextData connection $ encode (PriceSubscriptionMessage $ _coinId <$> coins)
  let unsubscribeFromCoins =
        W.sendClose connection $ encode PriceUnsubscribeMessage
  let tickerReceiver rates =
        receiver connection >>= \case
          WsRecvMessage message ->
            eitherDecodeStrict' message & either (pure . WsError . pack . show) \case
              TickerUnknown t -> pure $ WsError t
              TickerUnsubscribe -> pure $ WsDone
              TickerPrice (TickerStreamMessage tickerMessage@TM.TickerMessage {..}) ->
                let previousRate = maybe tickerMessage fst $ M.lookup _tickerId rates
                    newRates = M.insert _tickerId (tickerMessage, previousRate) rates
                 in do
                      case findCoinById _tickerId initialCoins of
                        Nothing -> pure ()
                        Just coin -> foldCoinWithRate coin (tickerMessage, previousRate)
                      tickerReceiver newRates
          WsRecvAbort -> pure WsDone
          WsRecvTimeout msg -> pure $ WsError msg

  subscribeToCoins initialCoins
  result <- tickerReceiver M.empty
  unsubscribeFromCoins
  pure result

withLatest :: forall a. IO a -> (IO a -> IO a -> IO ()) -> IO ()
withLatest waitFor act = do
  initial <- waitFor
  latest <- Ref.newIORef initial
  mvar <- C.newEmptyMVar

  let loop = forever do
        nextVal <- waitFor
        Ref.atomicWriteIORef latest nextVal
        C.putMVar mvar nextVal
  C.race_ loop (act (Ref.readIORef latest) (C.takeMVar mvar))

withCoinRates :: IO [Coin] -> (Coin -> (TM.TickerMessage, TM.TickerMessage) -> IO ()) -> IO ()
withCoinRates waitForCoins foldCoin = withLatest waitForCoins \getLatest getNext -> forever do
  coins <- getLatest
  fprintLn ("Subscribing to Stream for coins: " % stext) (intercalate "," $ _coinName <$> coins)

  res <-
    Right
      <$> W.runSecureClient "stream.coinmarketcap.com" 443 "/price/latest" (ws coins (receiverWithCoins getNext) foldCoin)
      & (handle @W.HandshakeException) (pure . Left . show)
      & (handle @W.ConnectionException) (pure . Left . show)

  case res of
    Left err -> do
      fprintLn ("Ticker Stream Connection failed: " % string) err
      countdown 5
    Right result -> case result of
      WsError err -> do
        fprintLn ("Ws Error: " % stext) err
        countdown 5
      WsDone -> pure ()
  where
    countdown :: Integer -> IO ()
    countdown time = sequence_ [fprintLn int n *> CT.delay 1000000 | n <- [time, time - 1 .. 1]]
