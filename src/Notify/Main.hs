module Main where

import Control.Monad (forever, void)
import DBus.Notify
  ( Body (Text),
    Client,
    Note (appName, body, summary),
    blankNote,
    connectSession,
    notify,
  )
import Data.Foldable (traverse_)
import Data.List.NonEmpty (NonEmpty)
import Data.Text (unpack)
import Formatting (sformat, stext, fixed, (%))
import Moontick.Internal.Coin (Coin (..))
import qualified Moontick.Internal.MessageChannel as MC
import qualified Moontick.Internal.Notification as N

newtype Session = Session Client

openSession :: IO Session
openSession = Session <$> connectSession

appNote :: Note
appNote = blankNote {appName = "Moontick"}

waitForAlerts :: MC.MessageQueue -> IO (NonEmpty N.Alert)
waitForAlerts messages = go
  where
    go =
      MC.awaitMessage messages >>= \case
        MC.MessageAlerts alerts -> pure alerts
        _ -> go

alertSystem :: Session -> [Coin] -> MC.MessageQueue -> IO ()
alertSystem session _ messages =
  forever $ waitForAlerts messages >>= traverse_ (showAlert session)
  where
    showAlert :: Session -> N.Alert -> IO ()
    showAlert (Session client) N.Alert {..} =
      let coinName = _coinName _alertCoin
          noteSummary = unpack $ sformat (stext % " Price") coinName
       in void $
            notify client $ case _alertRule of
              N.LowerThan lt ->
                appNote
                  { summary = noteSummary,
                    body =
                      Just $
                        Text $
                          unpack $
                            sformat (stext % " price has dropped below " % fixed 4) coinName lt
                  }
              N.GreaterThan gt ->
                appNote
                  { summary = noteSummary,
                    body =
                      Just $
                        Text $
                          unpack $
                            sformat (stext % " price has raised above " % fixed 4) coinName gt
                  }

main :: IO ()
main = do
  session <- openSession
  MC.runCLIApp $
    MC.withMessages
      [ alertSystem session
      ]
