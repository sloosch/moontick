module Main where

import Control.Monad (forever)
import qualified Moontick.Internal.MessageChannel as MC
import System.IO (BufferMode (NoBuffering), hSetBuffering, stdout)

main :: IO ()
main = do
  hSetBuffering stdout NoBuffering
  MC.runCLIApp $
    MC.withMessages
      [ \coins messages -> do
          putStrLn $ "Init Coins:" <> show coins
          forever $ MC.awaitMessage messages >>= print
      ]
