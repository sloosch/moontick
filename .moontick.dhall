let P = https://gitlab.com/sloosch/moontick/-/raw/master/prelude.dhall 

let config
    : P.Config
    = { coins =
          [ { id =
                2010
            , name =
                "ADA"
            , alerts =
                [ P.AlertRule.GreaterThan (P.AlertValueGen.Points [ 0.8 ])
                , P.AlertRule.GreaterThan
                    ( P.AlertValueGen.LinearSteps
                        { from = 1.0, to = 2.0, step = 0.1 }
                    )
                , P.AlertRule.LowerThan
                    ( P.AlertValueGen.Fibonacci
                        { from = 2.0, to = 1.0, ratios = 2 }
                    )
                ]
            }
          , { id =
                1567
            , name =
                "NANO"
            , alerts =
                [ P.AlertRule.GreaterThan (P.AlertValueGen.Points [ 6.2 ])
                , P.AlertRule.LowerThan (P.AlertValueGen.Points [ 5.5 ])
                ]
            }
          , { id = 74, name = "DOGE", alerts = P.noAlerts }
          , { id = 1027, name = "ETH", alerts = P.noAlerts }
          ]
      }

in  config
